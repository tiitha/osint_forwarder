#!/usr/bin/env ruby

class Splunk_sender
	def initialize(options = {})
		raise sprintf("%s disabled.",self.class.name) if options['disabled']
		@options = options
	end

	def send(msg = nil)
		return if msg.nil?
		LOGGER.debug(sprintf("sending to splunk, collector %s", @options['collector']))

		begin
			url = URI.parse(@options['collector'])

			http = Net::HTTP.new(url.host, url.port)
	 		http.use_ssl = true
	 		http.verify_mode = OpenSSL::SSL::VERIFY_NONE # Not cool! Change this if using in real life

			request = Net::HTTP::Post.new(@options['collector'])

			message = {
	 			'event' => msg,
				'host' => Socket.gethostname.downcase,
	 			'time' => Time.now.to_i
	 		}

			request.body = message.to_json
	 		request.add_field("Content-type", "application/json")
	 		request.add_field("Authorization", sprintf("Splunk %s",@options['apikey']))
	 
	 		response = http.request(request)
			LOGGER.debug(response)
		rescue Exception => e
			LOGGER.error(e.message)
		end


	end
end