#!/usr/bin/env ruby

class Slack_sender
	def initialize(options = {})
		raise sprintf("%s disabled.",self.class.name) if options['disabled']
		@options = options
	end

	def send(msg = nil)
		return if msg.nil?
		LOGGER.debug("sending to slack..")
		begin
			message = sprintf('{ "attachments": [ { 
				"color": "#36a64f", 
				"pretext": ":hatched_chick: *%4$s* @ %1$s: %2$s", 
				"text": "%3$s",
				"mrkdwn_in": [ "text", "pretext" ] 
				} ] }', msg[:created], msg[:uri], msg[:message], msg[:from] )

			uri = URI.parse(@options['hook'])

			http = Net::HTTP.new(uri.host, uri.port)
			http.use_ssl = true
			http.verify_mode = OpenSSL::SSL::VERIFY_NONE # Not cool! Change this if using in real life

			request = Net::HTTP::Post.new(@options['hook'])
			request.body = message
			request.add_field("Content-type", "application/json")

			response = http.request(request)
			LOGGER.debug(response)
		rescue Exception => e
			LOGGER.error(e.message)
		end
	end
end