#!/usr/bin/env ruby

class Stdout_sender
	def initialize(options = {})
		raise sprintf("%s disabled.",self.class.name) if options['disabled']
		@options = options
	end
	def send(msg = nil)
		return if msg.nil?
		LOGGER.debug("printing to stdout..")
		printf("- - - - -\n%4$s: %3$s\n%1$s @ %2$s\n", msg[:created], msg[:uri], msg[:message], msg[:from] )
	end
end