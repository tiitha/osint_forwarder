#!/usr/bin/env ruby
require 'logger'
require 'yaml'

properties_file = "properties.yaml"
options = YAML.load_file( properties_file )

LOGGER = Logger.new(STDOUT)
LOGGER.level = options["debug"] ? Logger::DEBUG : Logger::INFO
LOGGER.info("Starting OSINT Forwarder")

fetchers = []
senders  = []
interval = options['interval']

# Loading modules
LOGGER.debug("start loading sources..")
options['sources'].each do |source,settings|
	class_name = sprintf("%s_poller", source.capitalize)
	begin
		require './poll_'+source
		LOGGER.info(sprintf("Loaded source '%s'", class_name))
		fetcher = Object.const_get( class_name )

		LOGGER.debug("adding source object to list")
		fetchers << fetcher.new(settings)
	rescue Exception => e
		LOGGER.warn(e.message)
		LOGGER.warn(sprintf("Ignoring source '%s'", class_name))
	end
end

LOGGER.debug(sprintf("done with sources, %d poller(s) loaded.", fetchers.length))
LOGGER.debug("start loading destinations")

options['destinations'].each do |destination,settings|
	class_name = sprintf("%s_sender", destination.capitalize)
	begin
		require './send_'+destination
		LOGGER.info(sprintf("Loaded destination '%s'", class_name))
		sender = Object.const_get( class_name )

		LOGGER.debug("adding destination object to list")
		senders << sender.new(settings)
	rescue Exception => e
		LOGGER.warn(e.message)
		LOGGER.warn(sprintf("Ignoring destination '%s'", class_name))
	end
end

loop do
	# check fetchers for new messages
	fetchers.each do |fetcher|
		# get fetcher messages
		LOGGER.info(sprintf("getting messages from %s", fetcher.class.name))
		begin
			messages = fetcher.getNewMessages()
		rescue Exception => e
			LOGGER.error(e.message)
			messages = []
		end
		LOGGER.debug(sprintf("found %d new messages", messages.length))
		if messages.length > 0
			senders.each do |sender|
				# send message
				LOGGER.debug(sprintf("Sending message(s) to %s", sender.class.name))
				messages.each do |message|
					sender.send(message)
				end
			end
		end
	end
	sleep(interval)
end

LOGGER.info("Exiting.. ")

