#!/usr/bin/env ruby
require "net/http"
require "uri"
require "nokogiri"
require "time"
require "date"


class Rss_poller
	def initialize(options = {})
		raise sprintf("%s disabled.",self.class.name) if options['settings']['disabled']
		@options = options['settings']

		@feeds = {}
		feeds = options['feeds']
		state = _loadState()
		feeds.each do |feed|
			@feeds[feed] = state[feed] || 0
		end
		LOGGER.debug(sprintf("RSS initialized, following %d RSS feeds", @feeds.length))
	end

	def _loadState()
		state = {}
		state = File.open("rss.state") { |f| Marshal.load(f) } if File.exists?("rss.state")
		return state
	end

	def _saveState()
		File.open("rss.state", "w") { |f| Marshal.dump(@feeds, f) }
	end

	def _fetch(uri_str, limit = 10)
	  # You should choose better exception.
	  raise ArgumentError, 'HTTP redirect too deep' if limit == 0

	  url = URI.parse(uri_str)
	  useragent = @options['useragent']

		http = Net::HTTP.new(url.host, url.port)
		if url.scheme == 'https'
			http.use_ssl = true
			http.verify_mode = OpenSSL::SSL::VERIFY_NONE # Not cool! Change this if using in real life
		end

		request = Net::HTTP::Get.new(url)
		request.add_field("User-Agent", useragent)

		response = http.request(request)

	  case response
	  when Net::HTTPSuccess     then response
	  when Net::HTTPRedirection then _fetch(response['location'], limit - 1)
	  else
	    response.error!
	  end
	end


	def getNewMessages()
		messages = []

		@feeds.each do |feed, state|
			begin
				response = _fetch( feed )
			rescue Exception => e
				LOGGER.warn(sprintf("%s @ %s", e.message, feed))
			end
			next if response.nil?

			begin
				doc = Nokogiri::XML(response.body)

				max_id = state

				doc.css('item').each do |item| 
					ux_time = Time.parse(item.at_css('pubDate').content).to_i

					break if state > ux_time

					if ux_time > max_id
						messages << { :created =>  DateTime.strptime(ux_time.to_s,'%s').to_s, :from => "RSS", :uri => item.at_css('link').content, :message => item.at_css('title').content }
						max_id = ux_time
					end
				end
				@feeds[feed] = max_id

			rescue Exception => e
					LOGGER.warn(sprintf("%s @ %s", e.message,feed))
			end
		end
		_saveState()
		return messages
	end
end