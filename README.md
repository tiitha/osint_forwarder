# osint_forwarder
OSINT (Open Source Intelligence) Forwarder gathers news and events from various sources (rss, twitter) and forwards them to defined destinations (currently stdout, splunk and slack). The goal of this tool is to notify users about published news as they are published through every-day channels without the hassle of using the source apps (e.g. twitter or rss feed readers).

## how it works
The pollers are defined in ``'poll_'`` objects, senders are defined in ``'send_'`` objects. The objects are loaded dynamically and controlled through the properties.yaml file (source and destination sections). Both sources and destinations can be disabled through the properties file. Script generates ``'.state'`` files to hold the source status (to prevent re-publishing events).

## running
```
ruby ./osint_runner.rb
```
Default configuration works for RSS to get an idea. To get the twitter part working, you need to insert your Twitter API keys. 
