#!/usr/bin/env ruby
require 'rubygems'
require 'bundler/setup'
require 'twitter'

#require 'json'
#require 'net/http'
#require 'openssl'
#require 'socket'

class Twitter_poller
	def initialize(options = {})
		raise sprintf("%s disabled.",self.class.name) if options['settings']['disabled']
		@client  = Twitter::REST::Client.new( options['settings'] )
		@screen_names = options['feeds'] || []
		@last_id = _loadState()
		LOGGER.debug(sprintf("Twitter initialized, following feeds: %s", @screen_names.join(", ")))
	end

	def _loadState()
		state = 0
		state = File.open("twitter.state", "r").read.to_i if File.exists?("twitter.state")
		return state
	end

	def _saveState(max_id)
		@last_id = max_id
		f = File.open("twitter.state", "w")
		f.puts(max_id)
		f.close()
	end

	def getNewMessages()
		return if @screen_names.length == 0

		twitter_query = "from:" + @screen_names.join(' OR from:')

		LOGGER.debug( sprintf("sending query '%s' with last_id '%d' to Twitter", twitter_query, @last_id) )

		result = @client.search( twitter_query, {
			:since_id => @last_id,
			:count => 10	# TODO:: currently not respecting the count. 
			})

		LOGGER.debug( sprintf("got %d tweets. Going forward w. iteration.. ", result.count) )

		messages = []

		max_id = @last_id
		result.each do |tweet|
			LOGGER.debug(sprintf("found item %d", tweet.id))
			messages << { :created =>  tweet.created_at, :from => tweet.user.screen_name, :uri => tweet.uri, :message => tweet.full_text }
			max_id = tweet.id if tweet.id > max_id
		end
		_saveState(max_id)
		return messages
	end
end